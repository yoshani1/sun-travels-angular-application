import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationComponent } from './navigation.component';
import {RouterTestingModule} from '@angular/router/testing';
import {Component} from '@angular/core';
import {Location} from '@angular/common';
import {By} from '@angular/platform-browser';
import {DOMHelper} from '../../testing/dom-helper';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;
  let dh: DOMHelper<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationComponent, DummyComponent ],
      imports: [RouterTestingModule.withRoutes(
        [
          {path: 'request', component: DummyComponent},
          {path: 'contracts', component: DummyComponent},
          {path: 'hotels', component: DummyComponent}
        ]
      )]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be on / at beginning', function () {
    const location = TestBed.get(Location);
    expect(location.path()).toBe('');
  });

  it('should navigate to /request on clicking requests link', function () {
    const location = TestBed.get(Location);
    const links = fixture.debugElement.queryAll(By.css('a'));
    const nativeLink: HTMLButtonElement = links[1].nativeElement;
    nativeLink.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/request');
    });
  });

  it('should navigate to /contracts on clicking contracts link', function () {
    const location = TestBed.get(Location);
    const links = fixture.debugElement.queryAll(By.css('a'));
    const nativeLink: HTMLButtonElement = links[2].nativeElement;
    nativeLink.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/contracts');
    });
  });

  it('should navigate to /hotels on clicking hotels link', function () {
    const location = TestBed.get(Location);
    const links = fixture.debugElement.queryAll(By.css('a'));
    const nativeLink: HTMLButtonElement = links[3].nativeElement;
    nativeLink.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/hotels');
    });
  });

  it('should have one ul tag', function () {
    expect(dh.count('ul')).toEqual(1);
  });

  it('should have 3 list items', function () {
    expect(dh.count('li')).toEqual(3);
  });

});

@Component({template: ''})
class DummyComponent {
}
