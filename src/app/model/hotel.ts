export interface Hotel {
  id: any;
  hotelName: string;
  telephone: string;
  address: string;
}
