import {Hotel} from './hotel';
import {RoomType} from './room-type';

export interface AddHotel {
  hotelViewModel: Hotel;
  roomTypeViewModelList: RoomType[];
}
