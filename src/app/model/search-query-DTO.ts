export interface SearchQueryDTO {
  startDate: Date;
  noOfNights: number;
  roomPref: {
    noOfRooms: number;
    noOfAdults: number;
  }[];
}
