export interface RoomType {
  id: any;
  hotelId: any;
  type: string;
  maxAdults: number;
  noOfRooms: number;
}
