import {ContractRoomEntryDTO} from './contract-room-entry-DTO';

export interface ViewContract {
  hotelName: string;
  hotelAddress: string;
  startDate: Date;
  endDate: Date;
  contractRoomEntryDTOList: ContractRoomEntryDTO[];
}

