import {SearchResultTypePriceDTO} from './search-result-type-price-dto';

export interface SearchResult {
  hotelName: string;
  hotelAddress: string;
  searchResultRoomEntryDTOList: {
    searchResultRoomEntryList: SearchResultTypePriceDTO[];
    roomPrefDTO: {
      noOfRooms: number;
      noOfAdults: number;
    };
  }[];
}
