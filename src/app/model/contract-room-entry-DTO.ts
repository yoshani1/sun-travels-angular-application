export interface ContractRoomEntryDTO {
  roomType: string;
  price: number;
  noOfRooms: number;
  maxAdults: number;
}
