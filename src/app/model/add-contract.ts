import {Contract} from './contract';
import {ContractRoomType} from './contract-room-type';

export interface AddContract {
  contractViewModel: Contract;
  contractRoomTypeViewModelList: ContractRoomType[];
}
