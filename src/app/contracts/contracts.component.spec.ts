import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContractsComponent } from './contracts.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbModal, NgbPagination} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../shared/api.service';
import {FormsModule} from '@angular/forms';
import {ContractFilterPipe} from '../shared/contract-filter.pipe';
import {of} from 'rxjs';
import {DOMHelper} from '../../testing/dom-helper';
import {ViewContract} from '../model/view-contract';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

describe('ContractsComponent', () => {
  let component: ContractsComponent;
  let fixture: ComponentFixture<ContractsComponent>;
  let dh: DOMHelper<ContractsComponent>;
  let apiServiceMock: any;

  beforeEach(async(() => {

    apiServiceMock = jasmine.createSpyObj('ApiService', ['getAllContracts']);
    apiServiceMock.getAllContracts.and.returnValue(of([]));

    TestBed.configureTestingModule({
      declarations: [ ContractsComponent, ContractFilterPipe, NgbPagination ],
      imports: [RouterTestingModule, FormsModule],
      providers: [NgbModal,
        {provide: ApiService, useValue: apiServiceMock}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractsComponent);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);
    fixture.detectChanges();
  });

  describe('Basic HTML tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should contain one input field', function () {
      expect(dh.count('input')).toEqual(1);
    });

    it('input field should contain be empty at first', function () {
      expect(dh.singleText('input')).toBe('');
    });

    it('should contain two link buttons', function () {
      expect(dh.count('a')).toEqual(2);
    });

    it('should contain one button for search and one for add contract', function () {
      expect(dh.findTextByIndex('a', 0)).toBe(' Search');
      expect(dh.findTextByIndex('a', 1)).toBe(' New contract');
    });
  });

  describe('call NgOnInit on Demand', () => {
    it('should call getContracts on the ApiService one time on ngOnInit', () => {
      fixture.detectChanges();
      expect(apiServiceMock.getAllContracts).toHaveBeenCalledTimes(1);
    });
  });

  describe('main contracts display', () => {
    let helper: Helper;
    beforeEach(() => {
      helper = new Helper();
      fixture.detectChanges();
    });

    it('should contain no results image if contracts list is empty', function () {
      expect(dh.findAll('img')).toBeTruthy();
      expect(dh.count('img')).toEqual(1);
    });

    it('should display contracts if contracts list is not empty', function () {

      // set values for contracts list
      component.contracts = helper.getAllContracts();
      fixture.detectChanges();

      // two pagination elements
      expect(dh.findAll('ngb-pagination')).toBeTruthy();
      expect(dh.count('ngb-pagination')).toEqual(2);

      // one expired contract
      expect(dh.countText('div', 'Expired')).toEqual(1);

      // tests for HTML elements of one contract
      expect(dh.countText('div', 'hotel1')).toEqual(1);
      expect(dh.countText('div', 'address')).toEqual(1);
      expect(dh.singleText('p')).toEqual( 'Valid from 14-10-2019 to 14-10-2020');

      // table elements
      expect(dh.count('table')).toEqual(1);

      // table headers
      expect(dh.findTextByIndex('th', 0)).toBe('Room Type');
      expect(dh.findTextByIndex('th', 1)).toBe('Price (LKR)');
      expect(dh.findTextByIndex('th', 2)).toBe('Number of rooms');
      expect(dh.findTextByIndex('th', 3)).toBe('Maximum adults');

      // table data fields
      expect(dh.findTextByIndex('td', 0)).toBe('test type');
      expect(dh.findTextByIndex('td', 1)).toBe('2500');
      expect(dh.findTextByIndex('td', 2)).toBe('2');
      expect(dh.findTextByIndex('td', 3)).toBe('3');

      // footer
      expect(dh.countText('div', '*All above rates are per person, per night')).toEqual(1);

    });
  });

  describe('navigation to new contract page', () => {
    let location: Location;
    let router: Router;
    beforeEach(() => {
      location = TestBed.get(Location);
      router = TestBed.get(Router);
      fixture.detectChanges();
    });
    // navigation
    it('should navigate to /add-contract on new contract button click',
      () => {
        spyOn(router, 'navigateByUrl');
        dh.clickButtonLink(' New contract');
        expect(router.navigateByUrl)
          .toHaveBeenCalledWith(router.createUrlTree(['/contracts/add-contract']),
            { skipLocationChange: false, replaceUrl: false });
      });
  });

});


class Helper {
  contracts: ViewContract[] = [];

  contract1: ViewContract = {hotelName: 'hotel1', hotelAddress: 'address', startDate: new Date('2019-10-14'),
    endDate: new Date('2020-10-14'), contractRoomEntryDTOList: [{roomType: 'test type', price: 2500, noOfRooms: 2, maxAdults: 3}]};

  getAllContracts(): ViewContract[] {
    this.contracts.push(this.contract1);
    return this.contracts;
  }
}
