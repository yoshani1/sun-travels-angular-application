import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadContractComponent } from './load-contract.component';
import {DOMHelper} from '../../../testing/dom-helper';
import {of} from 'rxjs';
import {FormArray, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../shared/api.service';
import {Hotel} from '../../model/hotel';

describe('LoadContractComponent', () => {
  let component: LoadContractComponent;
  let fixture: ComponentFixture<LoadContractComponent>;
  let dh: DOMHelper<LoadContractComponent>;
  let apiServiceMock: any;

  beforeEach(async(() => {
    apiServiceMock = jasmine.createSpyObj('ApiService', ['getAllHotels', 'postContract', 'getRoomTypesByHotel']);
    apiServiceMock.getAllHotels.and.returnValue(of([]));
    apiServiceMock.postContract.and.returnValue(of([]));
    apiServiceMock.getRoomTypesByHotel.and.returnValue(of([]));

    TestBed.configureTestingModule({
      declarations: [ LoadContractComponent ],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        FormBuilder,
        NgbModal,
        {provide: ApiService, useValue: apiServiceMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadContractComponent);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);
    fixture.detectChanges();
  });
  describe('Basic HTML tests', () => {

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should contain p tag add a contract', function () {
      expect(dh.singleText('p')).toBe('Add a contract');
    });

    it('two buttons including save and room entry add button should exist', function () {
      const buttons = dh.findAll('button');
      expect(buttons.length === 2 ).toBeTruthy();
    });

    it('should have 3 input fields at first', function () {
      expect(dh.count('input')).toEqual(3);
    });

    it('should have 1 select field at first', function () {
      expect(dh.count('select')).toEqual(1);
    });

    it('should be a button \'Add room entry\' first on page', function () {
      const roomAddBtn = dh.singleText('button');
      expect(roomAddBtn).toBe(' Add room entry ');
    });

    it('should have a save on page', function () {
      expect(dh.findTextByIndex('button', 1)).toBe(' Save ');
    });
  });

  describe('call NgOnInit on Demand', () => {
    it('should call getHotels on the ApiService one time on ngOnInit', () => {
      fixture.detectChanges();
      expect(apiServiceMock.getAllHotels).toHaveBeenCalledTimes(1);
    });
  });

  describe('Parent form group validation tests', () => {
    it('form invalid when empty', () => {
      expect(component.parentForm.valid).toBeFalsy();
    });

    it('start date field validity', () => {
      const startDate = component.parentForm.controls['startDate'];
      expect(startDate.valid).toBeFalsy();

      // empty start date
      startDate.setValue('');
      expect(startDate.hasError('required')).toBeTruthy();

      // non-empty date accepted
      startDate.setValue(new Date('2021-10-15'));
      expect(component.parentForm.hasError('invalidStartDate')).toBeFalsy();
    });

    it('end date field validity', () => {
      const endDate = component.parentForm.controls['endDate'];
      expect(endDate.valid).toBeFalsy();

      // empty end date
      endDate.setValue('');
      expect(endDate.hasError('required')).toBeTruthy();

      // past date invalid
      endDate.setValue(new Date('2020-10-14'));
      expect(component.parentForm.hasError('invalidEndDate')).toBeTruthy();

      // future date accepted
      endDate.setValue(new Date('2021-10-15'));
      expect(component.parentForm.hasError('invalidEndDate')).toBeFalsy();
    });

    it('end date cannot precede start date', function () {
      const startDate = component.parentForm.controls['startDate'];
      const endDate = component.parentForm.controls['endDate'];

      startDate.setValue(new Date('2021-10-15'));
      endDate.setValue(new Date('2020-10-14'));

      expect(component.parentForm.hasError('invalidDates')).toBeFalsy();
    });

    it('should have valid markup', function () {
      const markup = component.parentForm.controls['markup'];

      // initial value is 15
      expect(markup.value).toEqual(15);

      // empty markup
      markup.setValue('');
      expect(markup.hasError('required')).toBeTruthy();

      // markup > 100
      markup.setValue('110');
      expect(markup.hasError('max')).toBeTruthy();

      // markup < 0
      markup.setValue('-1');
      expect(markup.hasError('min')).toBeTruthy();

      // valid markup
      markup.setValue('18');
      expect(markup.hasError('invalid')).toBeFalsy();

    });

    it('should display fetched hotels in dropdown', function () {
      // set hotel
      const dummyHotel: Hotel = {hotelName: 'test hotel', address: 'test address', id: '1', telephone: '0741284920'};
      component.hotels = [dummyHotel];
      fixture.detectChanges();
      const selectedHotel = component.parentForm.controls['selectedHotel'];
      expect(selectedHotel.valid).toBeFalsy();

      // hotel not selected
      selectedHotel.setValue('');
      expect(selectedHotel.hasError('required')).toBeTruthy();

      const select: HTMLSelectElement = dh.findAll('#select-hotel')[0].nativeElement;
      select.value = select.options[0].value;
      select.dispatchEvent(new Event('change'));
      fixture.detectChanges();
      expect(dh.countText('#select-hotel', 'test hotel, test address')).toEqual(1);
      expect(apiServiceMock.getRoomTypesByHotel).toHaveBeenCalledTimes(1);
      expect(component.selectedHotel).toEqual(dummyHotel);
    });
  });

  describe('Form group of form array validation tests', () => {

    it('should validate form group', function () {

      // set hotel
      const dummyHotel: Hotel = {hotelName: 'test hotel', address: 'test address', id: '1', telephone: '0741284920'};
      component.hotels = [dummyHotel];
      fixture.detectChanges();
      const selectHotel: HTMLSelectElement = dh.findAll('#select-hotel')[0].nativeElement;
      selectHotel.value = selectHotel.options[0].value;
      selectHotel.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      // set room type
      component.roomTypes = [{type: 'test type', maxAdults: 2, noOfRooms: 5, hotelId: '1', id: '2'}];

      // add room entry
      const roomEntryForm = component.parentForm.get('roomEntries') as FormArray;

      dh.clickButton(' Add room entry ');
      fixture.detectChanges();

      // validate room type
      const roomType = roomEntryForm.controls[0].get('roomType');
      roomType.setValue('');
      expect(roomType.hasError('required')).toBeTruthy();

      const selectRoom1: HTMLSelectElement = dh.findAll('#select-room-type')[0].nativeElement;
      selectRoom1.value = selectRoom1.options[0].value;
      selectRoom1.dispatchEvent(new Event('change'));
      fixture.detectChanges();
      expect(dh.countText('#select-room-type', 'test type with 2 maximum adults')).toEqual(1);

      // validate number of rooms
      const noOfRooms = roomEntryForm.controls[0].get('noOfRooms');

      // check if maximum number of rooms was set automatically
      expect(noOfRooms.value).toEqual(component.roomTypes[0].noOfRooms);

      noOfRooms.setValue('');
      expect(noOfRooms.hasError('required')).toBeTruthy();

      noOfRooms.setValue(0);
      expect(noOfRooms.hasError('min')).toBeTruthy();

      // cannot exceed physical number of rooms
      noOfRooms.patchValue(component.roomTypes[0].noOfRooms + 1);
      expect(roomEntryForm.controls[0].hasError('invalidRooms')).toBeTruthy();

      // validate price
      const price = roomEntryForm.controls[0].get('price');

      price.setValue('');
      expect(price.hasError('required')).toBeTruthy();

      price.setValue(0);
      expect(price.hasError('min')).toBeTruthy();

      // no same room type in two entries
      dh.clickButton(' Add room entry '); // add another form group
      fixture.detectChanges();

      // set same value as before
      const selectRoom2: HTMLSelectElement = dh.findAll('#select-room-type')[1].nativeElement;
      selectRoom2.value = selectRoom2.options[0].value;
      selectRoom2.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      expect(roomEntryForm.hasError('invalidRoomType')).toBeTruthy();

      // find and click one close button
      const closeButton = dh.findButton('×');
      expect(closeButton).toBeTruthy();

      dh.clickButtonAtIndex('×', 1);
      fixture.detectChanges();
      expect(roomEntryForm.length).toBe(1);

      // set missing data
      component.parentForm.controls['startDate'].patchValue(new Date('2020-10-15'));
      component.parentForm.controls['endDate'].patchValue(new Date('2021-10-15'));
      roomEntryForm.controls[0].get('price').patchValue(3000);

      fixture.detectChanges();

      // validate form
      expect(component.parentForm.valid).toBeTruthy();

    });

  });



});
