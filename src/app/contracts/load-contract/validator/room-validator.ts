import {AbstractControl} from '@angular/forms';

export function RoomValidator(control: AbstractControl): { [ key: string]: boolean} | null {

  const maxRooms = control.get('roomType').value['noOfRooms'];
  const noOfRooms = control.get('noOfRooms').value;

  return noOfRooms > maxRooms ? {invalidRooms: true} : null;

}
