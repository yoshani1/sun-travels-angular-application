import {AbstractControl} from '@angular/forms';

export function DateValidator(control: AbstractControl): { [ key: string]: boolean} | null {

  const startDate = control.get('startDate');
  const endDate = control.get('endDate');
  const today = new Date();
  today.setHours(0, 0, 0, 0);

  if (new Date(endDate.value) < today ) {
    return {
      invalidEndDate: true
    };
  }
  if (startDate.value > endDate.value ) {
    return {
      invalidDates: true
    };
  }
}
