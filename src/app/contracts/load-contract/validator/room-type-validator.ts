import {AbstractControl, ValidatorFn} from '@angular/forms';

export function RoomTypeValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {

    const len = control.value.length;
    const arr = [];
    for (let i = 0; i < len; i ++) {
      arr.push(control.value[i]['roomType']);
    }

    if (new Set(arr).size < arr.length) {
      return {
        invalidRoomType: true
      };
    }
  };
}

