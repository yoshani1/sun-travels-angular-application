import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../shared/api.service';
import {Hotel} from '../../model/hotel';
import {RoomType} from '../../model/room-type';
import {Contract} from '../../model/contract';
import {ContractRoomType} from '../../model/contract-room-type';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {DateValidator} from './validator/date-validator';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContentComponent} from '../../shared/ngbd-modal-content/ngbd-modal-content.component';
import {RoomValidator} from './validator/room-validator';
import {RoomTypeValidator} from './validator/room-type-validator';

@Component({
  selector: 'app-load-contract',
  templateUrl: './load-contract.component.html',
  styleUrls: ['./load-contract.component.css']
})
export class LoadContractComponent implements OnInit {
  parentForm: FormGroup;
  hotels: Hotel[] = [];
  selectedHotel: Hotel;
  roomTypes: RoomType[] = [];
  contract: Contract = {
    contractId: '00000000-0000-0000-0000-000000000000',
    hotelId: null,
    startDate: null,
    endDate: null,
    markup: null
  };
  contractRoomTypeList: ContractRoomType[] = [];

  constructor(private apiService: ApiService, private fb: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.getAllHotels();
    this.parentForm = this.fb.group({
      selectedHotel: [null, [Validators.required]],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      markup: [15, [Validators.required, Validators.min(0), Validators.max(100)]],
      roomEntries: new FormArray([], [Validators.required])
    }, {validator: DateValidator});
  }

  public getAllHotels() {
    this.apiService.getAllHotels().subscribe(
      res => {
        this.hotels = res;
      },
      () => {
        this.open('Error', 'Oops! There seems to be an error', '#ff8080');
      }
    );
  }

  selectHotel(e) {
    this.selectedHotel = this.hotels.find(n => n.id === e.target.value);

    // set hotel id of contract
    this.contract.hotelId = this.selectedHotel.id;

    this.apiService.getRoomTypesByHotel(this.selectedHotel.id).subscribe(
      res => {
        this.roomTypes = res;
      },
      () => {this.open('Error', 'Oops! There seems to be an error', '#ff8080'); }
    );
  }


// room entry functions
  get roomEntryForms() {
    return this.parentForm.get('roomEntries') as FormArray;
  }

  addRoomEntry() {
    const roomEntry = this.fb.group({
      roomType: [[], [Validators.required]],
      noOfRooms: [[], [Validators.required, Validators.min(1)]],
      price: [[], [Validators.required, Validators.min(1)]],
    }, {validator: RoomValidator});

    this.roomEntryForms.push(roomEntry);
  }

  deleteRoomEntry(i) {
    this.roomEntryForms.removeAt(i);
  }

  onRoomTypeSelect(i) {
    this.roomEntryForms.controls[i].get('noOfRooms').patchValue(this.roomEntryForms.value[i]['roomType']['noOfRooms']);
    this.roomEntryForms.setValidators(RoomTypeValidator());
    this.roomEntryForms.updateValueAndValidity();
  }

  // get form controls
  get selectedHotelValue() {
    return this.parentForm.get('selectedHotel');
  }

  get startDate() {
    return this.parentForm.get('startDate');
  }

  get endDate() {
    return this.parentForm.get('endDate');
  }

  get markup() {
    return this.parentForm.get('markup');
  }

  // submit contract
  submitContract() {
    const fa = this.roomEntryForms;
    console.log('submitted');
    this.contract.startDate = this.startDate.value;
    this.contract.endDate = this.endDate.value;
    this.contract.markup = this.markup.value;

    for (let i = 0; i < fa.length; i++) {
      this.contractRoomTypeList.push({
        roomTypeId : this.roomEntryForms.value[i]['roomType']['id'],
        contractId : '00000000-0000-0000-0000-000000000000',
        noOfRooms : this.roomEntryForms.value[i]['noOfRooms'],
        price : this.roomEntryForms.value[i]['price']
      });
    }
    this.postContract();
    this.contractRoomTypeList = [];
    this.resetParentForm();
  }

  postContract() {
    this.apiService.postContract({contractViewModel: this.contract, contractRoomTypeViewModelList: this.contractRoomTypeList}).subscribe(
      () => {
        this.open('Success', 'The contract was submitted successfully', '#b3ff99');
        setTimeout(function() {location.reload(); }, 3000);
        } ,
      () => { this.open('Error', 'Contract could not be uploaded', '#ff8080'); }
    );
  }

  resetParentForm() {
    this.parentForm.reset();
  }

  open(title, message, color) {
    const modalRef = this.modalService.open(NgbdModalContentComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.color = color;
  }

}

