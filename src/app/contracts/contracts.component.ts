import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {ViewContract} from '../model/view-contract';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContentComponent} from '../shared/ngbd-modal-content/ngbd-modal-content.component';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.css']
})
export class ContractsComponent implements OnInit {
  searchText: string;
  contracts: ViewContract[] = [];
  today: Date = new Date();
  // pagination
  currentPage = 1;
  itemsPerPage = 4;
  pageSize: number;

  constructor(private router: Router, private apiService: ApiService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getAllContracts();
    this.today.setHours(0, 0, 0, 0);
  }

  getAllContracts() {
    this.apiService.getAllContracts().subscribe(
      res => {
        this.contracts = res;
      },
      () => {
        this.open('Error', 'Oops! There seems to be an error', '#ff8080');
      }
    );
  }

  parseDate(date: any) {
    return Date.parse(date);
  }

// Check if the router url contains the specified route

  hasRoute(route: string) {
    return this.router.url.includes(route);
  }

  // open alert box
  open(title, message, color) {
    const modalRef = this.modalService.open(NgbdModalContentComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.color = color;
  }

  // pagination
  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }


}
