import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../shared/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContentComponent} from '../shared/ngbd-modal-content/ngbd-modal-content.component';
import {RoomType} from '../model/room-type';
import {Hotel} from '../model/hotel';
import {RoomAdultsValidator} from './validator/room-adults-validator';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
  parentForm: FormGroup;
  hotel: Hotel = {
    id: '00000000-0000-0000-0000-000000000000',
    hotelName: null,
    address: null,
    telephone: null
  };
  roomTypeList: RoomType[] = [];

  constructor(private apiService: ApiService, private fb: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.parentForm = this.fb.group({
      hotelName: [null, [Validators.required]],
      telephone: [null, [Validators.required, Validators.pattern(/^[0-9]\d*$/)]],
      address: [null, [Validators.required]],
      roomEntries: new FormArray([], Validators.required)
    });
  }

  // room entry functions
  get roomEntryForms() {
    return this.parentForm.get('roomEntries') as FormArray;
  }

  addRoomEntry() {
    const roomEntry = this.fb.group({
      roomType: [[], [Validators.required]],
      maxAdults: [[], [Validators.required, Validators.min(1)]],
      noOfRooms: [[], [Validators.required, Validators.min(1)]]
    });
    this.roomEntryForms.push(roomEntry);
  }

  deleteRoomEntry(i) {
    this.roomEntryForms.removeAt(i);
  }

  noDuplicates() {
    this.roomEntryForms.setValidators(RoomAdultsValidator());
    this.roomEntryForms.updateValueAndValidity();
  }

  // get form controls
  get hotelName() {
    return this.parentForm.get('hotelName');
  }

  get telephone() {
    return this.parentForm.get('telephone');
  }

  get address() {
    return this.parentForm.get('address');
  }

  // submit hotel
  submitHotel() {
    const fa = this.roomEntryForms;
    console.log('submitted');
    this.hotel.hotelName = this.hotelName.value;
    this.hotel.address = this.address.value;
    this.hotel.telephone = this.telephone.value;

    for (let i = 0; i < fa.length; i++) {
      this.roomTypeList.push({
        id : '00000000-0000-0000-0000-000000000000',
        hotelId : '00000000-0000-0000-0000-000000000000',
        type : this.roomEntryForms.value[i]['roomType'],
        maxAdults : this.roomEntryForms.value[i]['maxAdults'],
        noOfRooms: this.roomEntryForms.value[i]['noOfRooms']
      });
    }
    this.postHotel();
    this.roomTypeList = [];
  }

  postHotel() {
    this.apiService.postHotel({hotelViewModel: this.hotel, roomTypeViewModelList: this.roomTypeList}).subscribe(
      () => {
        this.open('Success', 'The hotel was submitted successfully', '#b3ff99');
        this.resetParentForm();
        setTimeout(function() {location.reload(); }, 3000);
      } ,
      () => { this.open('Error', 'Hotel could not be uploaded', '#ff8080'); }
    );
  }

  resetParentForm() {
    this.parentForm.reset();
  }

  open(title, message, color) {
    const modalRef = this.modalService.open(NgbdModalContentComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.color = color;
  }
}
