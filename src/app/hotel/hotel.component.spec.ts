import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelComponent } from './hotel.component';
import {DOMHelper} from '../../testing/dom-helper';
import {of} from 'rxjs';
import {FormArray, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../shared/api.service';
import {NgbdModalContentComponent} from '../shared/ngbd-modal-content/ngbd-modal-content.component';

describe('HotelComponent', () => {
  let component: HotelComponent;
  let fixture: ComponentFixture<HotelComponent>;
  let dh: DOMHelper<HotelComponent>;
  let apiServiceMock: any;

  beforeEach(async(() => {
    apiServiceMock = jasmine.createSpyObj('ApiService', ['postHotel']);
    apiServiceMock.postHotel.and.returnValue(of([]));


    TestBed.configureTestingModule({
      declarations: [ HotelComponent, NgbdModalContentComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        FormBuilder,
        NgbModal,
        {provide: ApiService, useValue: apiServiceMock},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelComponent);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);
    fixture.detectChanges();
  });

  describe('Basic HTML tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should contain p tag add a hotel', function () {
      expect(dh.singleText('p')).toBe('Add a hotel');
    });

    it('two buttons including submit and add room type add button should exist', function () {
      const buttons = dh.findAll('button');
      expect(buttons.length === 2 ).toBeTruthy();
    });

    it('should be a button \'Add room type\' first on page', function () {
      const roomAddBtn = dh.singleText('button');
      expect(roomAddBtn).toBe(' Add room type ');
    });

    it('should have a submit on page', function () {
      expect(dh.findTextByIndex('button', 1)).toBe(' Submit ');
    });

    it('should contain minimum three inputs', function () {
      expect(dh.count('input')).toBeGreaterThanOrEqual(3);
    });
  });

  describe('Parent form group validation tests', () => {

    it('form invalid when empty', () => {
      expect(component.parentForm.valid).toBeFalsy();
    });

    it('hotel name field validity', () => {
      const hotelName = component.parentForm.controls['hotelName'];
      expect(hotelName.valid).toBeFalsy();

      // empty hotel name
      hotelName.setValue('');
      expect(hotelName.hasError('required')).toBeTruthy();
    });

    it('telephone field validity', function () {

      const telephone = component.parentForm.controls['telephone'];
      expect(telephone.valid).toBeFalsy();

      // empty telephone number
      telephone.setValue('');
      expect(telephone.hasError('required')).toBeTruthy();

      // non numerical telephone
      telephone.setValue('qwt-5');
      expect(telephone.hasError('pattern')).toBeTruthy();
    });

    it('address field validity', () => {
      const address = component.parentForm.controls['address'];
      expect(address.valid).toBeFalsy();

      // empty address
      address.setValue('');
      expect(address.hasError('required')).toBeTruthy();
    });
  });

  describe('Form array validation tests', () => {
    it('should parent form be invalid at first', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      expect(roomEntries.valid).toBeFalsy();
    });

    it('should add one room type form group when add button clicked', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      dh.clickButton(' Add room type ');
      fixture.detectChanges();
      expect(roomEntries.value.length).toBe(1);
    });

    it('should be two room type form groups when add button clicked twice', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      dh.clickButton(' Add room type ');
      dh.clickButton(' Add room type ');
      fixture.detectChanges();
      expect(roomEntries.value.length).toBe(2);
    });
  });

  describe('Form group of form array validation tests', () => {
    it('should validate form group', function () {
      const roomEntryForm = component.parentForm.get('roomEntries') as FormArray;
      dh.clickButton(' Add room type ');
      fixture.detectChanges();

      // validate max adults
      const maxAdults = roomEntryForm.controls[0].get('maxAdults');

      maxAdults.setValue('');
      expect(maxAdults.hasError('required')).toBeTruthy();

      maxAdults.setValue(0);
      expect(maxAdults.hasError('min')).toBeTruthy();

      // validate room type
      const roomType = roomEntryForm.controls[0].get('roomType');

      roomType.setValue('');
      expect(roomType.hasError('required')).toBeTruthy();

      // validate physical number of rooms
      const noOfRooms = roomEntryForm.controls[0].get('noOfRooms');

      noOfRooms.setValue('');
      expect(noOfRooms.hasError('required')).toBeTruthy();

      noOfRooms.setValue(0);
      expect(noOfRooms.hasError('min')).toBeTruthy();

      // validate no same combination of maximum adults and room type in two entries
      dh.clickButton(' Add room type '); // add another form group
      fixture.detectChanges();

      // set same values for combination
      roomEntryForm.controls[0].get('roomType').setValue('Standard');
      roomEntryForm.controls[1].get('roomType').setValue('Standard');

      roomEntryForm.controls[0].get('maxAdults').setValue(2);
      roomEntryForm.controls[1].get('maxAdults').setValue(2);

      fixture.detectChanges();
      component.noDuplicates();
      expect(roomEntryForm.hasError('invalidRoomAdultCombination')).toBeTruthy();

      // find and click one close button
      const closeButton = dh.findButton('×');
      expect(closeButton).toBeTruthy();

      dh.clickButton('×');
      fixture.detectChanges();
      expect(roomEntryForm.length).toBe(1);

    });
  });
});
