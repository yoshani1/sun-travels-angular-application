import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-modal-content',
  templateUrl: './ngbd-modal-content.component.html',
  styleUrls: ['./ngbd-modal-content.component.css']
})
export class NgbdModalContentComponent {
  @Input() title;
  @Input() message;
  @Input() color;

  constructor(public activeModal: NgbActiveModal) { }

onClose() {
  this.activeModal.close('Close click');
}

}
