import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbdModalContentComponent } from './ngbd-modal-content.component';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DOMHelper} from '../../../testing/dom-helper';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';

describe('NgbdModalContentComponent', () => {
  let component: NgbdModalContentComponent;
  let fixture: ComponentFixture<NgbdModalContentComponent>;
  let dh: DOMHelper<NgbdModalContentComponent>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [NgbdModalContentComponent],
        providers: [NgbActiveModal, NgbModal],
      })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [NgbdModalContentComponent] } });
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(NgbdModalContentComponent);
    component = fixture.componentInstance;
    component.title = 'test alert';
    component.message = 'test message';
    fixture.detectChanges();
    dh = new DOMHelper(fixture);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should contain an h4 tag displaying title', () => {
    expect(dh.singleText('h4')).toBe('test alert');
  });

  it('should be two buttons on modal', () => {
    expect(dh.count('button')).toEqual(2);
  });

  it('should be a close button first on the modal', () => {
    expect(dh.singleText('button')).toBe('×');
  });

  it('should contain p tag displaying message', () => {
    expect(dh.singleText('p')).toBe('test message!');
  });

});
