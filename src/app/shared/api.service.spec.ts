import {getTestBed, TestBed} from '@angular/core/testing';

import { ApiService } from './api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Hotel} from '../model/hotel';
import {RoomType} from '../model/room-type';
import {AddHotel} from '../model/add-hotel';
import {AddContract} from '../model/add-contract';
import {ViewContract} from '../model/view-contract';
import {SearchQueryDTO} from '../model/search-query-DTO';

describe('ApiService', () => {
  let httpMock: HttpTestingController;
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });

    httpMock = getTestBed().get(HttpTestingController);
    apiService = getTestBed().get(ApiService);
  });

  it('it is created', () => {
    expect(apiService).toBeTruthy();
  });

  // get all hotels
  it('should get all hotels from http', () => {
    // arrange
    const dummyHotels: Hotel[] = [
      {id: '1', hotelName: 'hotel1', telephone: '0283910239', address: 'dummy address'},
      {id: '2', hotelName: 'hotel2', telephone: '0283910239', address: 'dummy address'},
      {id: '3', hotelName: 'hotel3', telephone: '0283910239', address: 'dummy address'}
    ];

    apiService.getAllHotels().subscribe(res => {
      expect(res.length).toBe(3);
      expect(res).toEqual(dummyHotels);
    });

    // http mock
    const req = httpMock.expectOne(apiService.ALL_HOTELS_URL);
    expect(req.request.method).toBe('GET');
    req.flush(dummyHotels);
  });

  // get room types
  it('should get all room types by hotel', function () {
    const dummyHotel: Hotel = {id: 'hid', hotelName: 'hotel1', telephone: '0283910239', address: 'dummy address'};

    const dummyRoomTypes: RoomType[] = [
      {id: '1', hotelId: 'hid', type: 'standard', noOfRooms: 2, maxAdults: 3}
    ];

    apiService.getRoomTypesByHotel('hid').subscribe(res => {
      expect(res.length).toBe(1);
      expect(res).toEqual(dummyRoomTypes);
    });

    const req = httpMock.expectOne(apiService.ROOM_TYPES_BY_HOTEL_URL + dummyHotel.id);
    expect(req.request.method).toBe('GET');
    req.flush(dummyRoomTypes);
  });

  // post hotel
  it('should post a new hotel', function () {
    const dummyHotelDTO: AddHotel = {hotelViewModel: {hotelName: '', address: '', telephone: '', id: ''}, roomTypeViewModelList: []};

    apiService.postHotel(dummyHotelDTO).subscribe(res => {
      expect(res).toBe(null);
    });

    const req = httpMock.expectOne(apiService.ADD_HOTEL);
    expect(req.request.method).toBe('POST');

  });

  // post contract
  it('should post a new contract', function () {
    const dummyContract: AddContract = {
      contractViewModel: {hotelId: '', markup: '', endDate: new Date(), startDate: new Date(), contractId: ''},
      contractRoomTypeViewModelList: []};

    apiService.postContract(dummyContract).subscribe(res => {
      expect(res).toBe(null);
    });

    const req = httpMock.expectOne(apiService.ADD_CONTRACT);
    expect(req.request.method).toBe('POST');

  });

  // get all contracts
  it('should get all contracts from http', () => {
    // arrange
    const dummyContracts: ViewContract[] = [
      {hotelName: '', hotelAddress: '', endDate: new Date(), startDate: new Date(), contractRoomEntryDTOList: []}
    ];

    apiService.getAllContracts().subscribe(res => {
      expect(res.length).toBe(1);
      expect(res).toEqual(dummyContracts);
    });

    // http mock
    const req = httpMock.expectOne(apiService.GET_CONTRACTS);
    expect(req.request.method).toBe('GET');
    req.flush(dummyContracts);
  });

  // post search
  it('should post a search query', function () {
    let searchQuery: SearchQueryDTO;
    searchQuery = {startDate: new Date(), noOfNights: 2, roomPref: [{noOfAdults: 3, noOfRooms: 2}]};

    apiService.postSearch(searchQuery).subscribe(res => {
      expect(res).toBe(null);
    });

    const req = httpMock.expectOne(apiService.POST_SEARCH);
    expect(req.request.method).toBe('POST');
  });

});
