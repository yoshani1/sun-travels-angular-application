import { ContractFilterPipe } from './contract-filter.pipe';
import {ViewContract} from '../model/view-contract';

describe('ContractFilterPipe', () => {
  it('create an instance', () => {
    const pipe = new ContractFilterPipe();
    expect(pipe).toBeTruthy();
  });

  it('should not filter contracts if search text is empty', () => {

    const pipe = new ContractFilterPipe();
    const emptySearchText = '';
    const contracts: ViewContract[] = [
      {hotelName: 'hotel1', hotelAddress: 'a', startDate: new Date('2020-10-22'),
        endDate: new Date('2020-10-25'), contractRoomEntryDTOList: []},
      {hotelName: 'hotel2', hotelAddress: 'b', startDate: new Date('2020-10-23'),
        endDate: new Date('2020-10-27'), contractRoomEntryDTOList: []}
    ];

    const filteredContracts = pipe.transform(contracts, emptySearchText);

    expect(filteredContracts.length).toBe(2);
  });

  it('should filter contracts based on search text', () => {

    const pipe = new ContractFilterPipe();
    const searchText = 'hotel2';
    const contracts: ViewContract[] = [
      {hotelName: 'hotel1', hotelAddress: 'a', startDate: new Date('2020-10-22'),
        endDate: new Date('2020-10-25'), contractRoomEntryDTOList: []},
      {hotelName: 'hotel2', hotelAddress: 'b', startDate: new Date('2020-10-23'),
        endDate: new Date('2020-10-27'), contractRoomEntryDTOList: []}
    ];

    const filteredContracts = pipe.transform(contracts, searchText);

    expect(filteredContracts.length).toBe(1);
    expect(filteredContracts[0].hotelName).toBe('hotel2');
  });
});
