import { Pipe, PipeTransform } from '@angular/core';
import {ViewContract} from '../model/view-contract';

@Pipe({
  name: 'contractFilter'
})
export class ContractFilterPipe implements PipeTransform {

  transform(contracts: ViewContract[], text: string): ViewContract[] {

    if (text == null || text === '') {
      return contracts;
    }
    const upperText = text.toUpperCase();
    return contracts.filter(name =>
      name.hotelName.toUpperCase().includes(upperText) || name.hotelAddress.toUpperCase().includes(upperText));

    // return listOfNames.filter(name =>
    //   name.toUpperCase().startsWith(upperLetter)
    // )
  }

}
