import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hotel} from '../model/hotel';
import {RoomType} from '../model/room-type';
import {AddContract} from '../model/add-contract';
import {ViewContract} from '../model/view-contract';
import {SearchQueryDTO} from '../model/search-query-DTO';
import {SearchResult} from '../model/search-result';
import {AddHotel} from '../model/add-hotel';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private BASE_URL = 'http://localhost:9191';
  ALL_HOTELS_URL = `${this.BASE_URL}/hotels`;
  ROOM_TYPES_BY_HOTEL_URL = `${this.BASE_URL}/byHotel/`;
  ADD_HOTEL = `${this.BASE_URL}/addHotel`;
  ADD_CONTRACT = `${this.BASE_URL}/addContract`;
  GET_CONTRACTS = `${this.BASE_URL}/allContracts`;
  POST_SEARCH = `${this.BASE_URL}/searchRooms`;

  constructor(private http: HttpClient) { }

  getAllHotels(): Observable<Hotel[]> {
    return this.http.get<Hotel[]>(this.ALL_HOTELS_URL);
  }

  getRoomTypesByHotel(hotelId: any): Observable<RoomType[]> {
    return this.http.get<RoomType[]>(this.ROOM_TYPES_BY_HOTEL_URL + hotelId);
  }

  postHotel(addHotel: AddHotel): Observable<AddHotel> {
    return this.http.post<AddHotel>(this.ADD_HOTEL, addHotel);
  }

  postContract(addContract: AddContract): Observable<AddContract> {
    return this.http.post<AddContract>(this.ADD_CONTRACT, addContract);
  }

  getAllContracts(): Observable<ViewContract[]> {
    return this.http.get<ViewContract[]>(this.GET_CONTRACTS);
  }

  postSearch(searchQuery: SearchQueryDTO): Observable<SearchResult[]> {
    return this.http.post<SearchResult[]>(this.POST_SEARCH, searchQuery);
  }


}
