import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../shared/api.service';
import {DateValidator} from './validator/date-validator';
import {SearchResult} from '../model/search-result';
import {SearchQueryDTO} from '../model/search-query-DTO';
import {NgbdModalContentComponent} from '../shared/ngbd-modal-content/ngbd-modal-content.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AdultNumberValidator} from './validator/adult-number-validator';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  parentForm: FormGroup;
  searchQuery: SearchQueryDTO = {
    startDate: new Date(),
    noOfNights: null,
    roomPref: null
  };
  searchResults: SearchResult[];

  constructor(private apiService: ApiService, private fb: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.parentForm = this.fb.group({
      startDate: [null, [Validators.required]],
      noOfNights: [null, [Validators.required, Validators.min(1)]],
      roomEntries: new FormArray([], Validators.required)
    }, {validator: DateValidator});
  }

  // room entry functions
  get roomEntryForms() {
    return this.parentForm.get('roomEntries') as FormArray;
  }

  addRoomEntry() {
    const roomEntry = this.fb.group({
      noOfRooms: [[], [Validators.required, Validators.min(1)]],
      noOfAdults: [[], [Validators.required, Validators.min(1)]],
    });
    this.roomEntryForms.push(roomEntry);
  }

  deleteRoomEntry(i) {
    this.roomEntryForms.removeAt(i);
  }

  noDuplicates() {
    this.roomEntryForms.setValidators(AdultNumberValidator());
    this.roomEntryForms.updateValueAndValidity();
  }

  get startDate() {
    return this.parentForm.get('startDate');
  }

  get noOfNights() {
    return this.parentForm.get('noOfNights');
  }

  submitSearch() {
    this.searchQuery.startDate = new Date(this.startDate.value);
    this.searchQuery.noOfNights = this.noOfNights.value;
    this.searchQuery.roomPref = this.roomEntryForms.value;
    this.apiService.postSearch(this.searchQuery).subscribe(
      res => {
        this.searchResults = res;
      },
      () => { this.open('Error', 'Search could not be processed', '#ff8080'); }
    );
  }

  open(title, message, color) {
    const modalRef = this.modalService.open(NgbdModalContentComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.color = color;
  }

}
