import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RequestComponent} from './request.component';
import {FormArray, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiService} from '../shared/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DOMHelper} from '../../testing/dom-helper';
import {of} from 'rxjs';
import {SearchResult} from '../model/search-result';

describe('RequestComponent', () => {
  let component: RequestComponent;
  let fixture: ComponentFixture<RequestComponent>;
  let dh: DOMHelper<RequestComponent>;
  let apiServiceMock: any;

  beforeEach(async(() => {

    apiServiceMock = jasmine.createSpyObj('ApiService', ['postSearch']);
    apiServiceMock.postSearch.and.returnValue(of([]));

    TestBed.configureTestingModule({
      declarations: [ RequestComponent ],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        FormBuilder,
        NgbModal,
        {provide: ApiService, useValue: apiServiceMock}
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestComponent);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);

    fixture.detectChanges();
  });

  describe('Basic HTML tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should contain p tag search for rooms', function () {
      expect(dh.singleText('p')).toBe('Search for rooms');
    });

    it('two buttons including search and room entry add button should exist', function () {
        const buttons = dh.findAll('button');
        expect(buttons.length === 2 ).toBeTruthy();
    });

    it('should be a button \'Add room entry\' first on page', function () {
      const roomAddBtn = dh.singleText('button');
      expect(roomAddBtn).toBe(' Add room entry ');
    });

    it('should have a search on page', function () {
      expect(dh.findTextByIndex('button', 1)).toBe(' Search ');
    });

    it('should contain minimum two inputs', function () {
      expect(dh.count('input')).toBeGreaterThanOrEqual(2);
    });

  });

  describe('Parent form group validation tests', () => {
    it('form invalid when empty', () => {
      expect(component.parentForm.valid).toBeFalsy();
    });

    it('start date field validity', () => {
      const startDate = component.parentForm.controls['startDate'];
      expect(startDate.valid).toBeFalsy();

      // empty start date
      startDate.setValue('');
      expect(startDate.hasError('required')).toBeTruthy();

      // past date invalid
      startDate.setValue(new Date('2020-10-14'));
      expect(component.parentForm.hasError('invalidStartDate')).toBeTruthy();

      // future date accepted
      startDate.setValue(new Date('2021-10-15'));
      expect(component.parentForm.hasError('invalidStartDate')).toBeFalsy();
    });


    it('no. of nights field validity', function () {

      const noOfNights = component.parentForm.controls['noOfNights'];
      expect(noOfNights.valid).toBeFalsy();

      // empty number of nights
      noOfNights.setValue('');
      expect(noOfNights.hasError('required')).toBeTruthy();

      // minimum number of nights
      noOfNights.setValue(0);
      expect(noOfNights.hasError('min')).toBeTruthy();
    });

  });

  describe('Form array validation tests', () => {

    it('should parent form be invalid at first', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      expect(roomEntries.valid).toBeFalsy();
    });

    it('should add one room entry form group when add button clicked', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      dh.clickButton(' Add room entry ');
      fixture.detectChanges();
      expect(roomEntries.value.length).toBe(1);
    });

    it('should be two room entry form groups when add button clicked twice', function () {
      const roomEntries = component.parentForm.controls['roomEntries'];
      dh.clickButton(' Add room entry ');
      dh.clickButton(' Add room entry ');
      fixture.detectChanges();
      expect(roomEntries.value.length).toBe(2);
    });
  });

  describe('Form group of form array validation tests', () => {

    it('should validate form group', function () {

      const roomEntryForm = component.parentForm.get('roomEntries') as FormArray;
      dh.clickButton(' Add room entry ');
      fixture.detectChanges();


      // validate number of adults
      const noOfAdults = roomEntryForm.controls[0].get('noOfAdults');

      noOfAdults.setValue('');
      expect(noOfAdults.hasError('required')).toBeTruthy();

      noOfAdults.setValue(0);
      expect(noOfAdults.hasError('min')).toBeTruthy();

      // validate number of rooms
      const noOfRooms = roomEntryForm.controls[0].get('noOfRooms');

      noOfRooms.setValue('');
      expect(noOfRooms.hasError('required')).toBeTruthy();

      noOfRooms.setValue(0);
      expect(noOfRooms.hasError('min')).toBeTruthy();

      // validate no same number of adults in two entries
      dh.clickButton(' Add room entry '); // add another form group
      fixture.detectChanges();

      const noOfAdults2 = roomEntryForm.controls[1].get('noOfAdults');
      noOfAdults.patchValue(2);  // set both numbers to equal values
      noOfAdults2.patchValue(2);
      component.noDuplicates();

      expect(roomEntryForm.hasError('invalidNoOfAdults')).toBeTruthy();

      // find and click one close button
      const closeButton = dh.findButton('×');
      expect(closeButton).toBeTruthy();

      dh.clickButton('×');
      fixture.detectChanges();
      expect(roomEntryForm.length).toBe(1);
    });

  });

  describe('Submit valid form', () => {
    let helper: Helper;
    beforeEach(() => {
      helper = new Helper();
      fixture.detectChanges();
    });

    it('should submit search and display results', function () {
      // set valid data
      component.startDate.patchValue(new Date('2020-10-25'));
      component.noOfNights.patchValue(2);
      dh.clickButton(' Add room entry ');
      fixture.detectChanges();
      const roomEntryForm = component.parentForm.get('roomEntries') as FormArray;
      roomEntryForm.controls[0].get('noOfAdults').patchValue(2);
      roomEntryForm.controls[0].get('noOfRooms').patchValue(1);

      component.submitSearch();
      expect(apiServiceMock.postSearch).toHaveBeenCalledTimes(1);

      component.searchResults = helper.getSearchResults();
      fixture.detectChanges();

      // one table should be displayed
      expect(dh.count('table')).toBe(1);

      // caption text
      expect(dh.singleText('caption')).toBe('Availability for 1 room(s) and 2 adult(s)');

      // table headers
      expect(dh.findTextByIndex('th', 0)).toBe('Room type');
      expect(dh.findTextByIndex('th', 1)).toBe('Price per room for 2 nights (LKR)');

      // table data fields
      expect(dh.findTextByIndex('td', 0)).toBe('test type');
      expect(dh.findTextByIndex('td', 1)).toBe('2,500');

    });

  });


});


class Helper {
  searchResults: SearchResult[] = [];
  getSearchResults(): SearchResult[] {

      this.searchResults.push(
        {hotelName: 'test hotel',
          hotelAddress: 'test address',
          searchResultRoomEntryDTOList: [{
            searchResultRoomEntryList: [{
              type: 'test type',
              markedUpPrice: 2500
            }],
            roomPrefDTO: {
              noOfAdults: 2,
              noOfRooms: 1
            }
        }]}
      );

    return this.searchResults;
  }
}




