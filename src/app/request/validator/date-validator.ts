import {AbstractControl} from '@angular/forms';

export function DateValidator(control: AbstractControl): { [ key: string]: boolean} | null {

  const startDate = control.get('startDate');
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  if (new Date(startDate.value) < today ) {
    return {
      invalidStartDate: true
    };
  }
}

