import {AbstractControl, ValidatorFn} from '@angular/forms';

export function AdultNumberValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {

    const len = control.value.length;
    const arr = [];
    for (let i = 0; i < len; i ++) {
        arr.push(control.value[i]['noOfAdults']);
    }

    if (new Set(arr).size < arr.length) {
      return {
        invalidNoOfAdults: true
      };
    }
  };
}
