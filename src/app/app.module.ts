import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ContractsComponent } from './contracts/contracts.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RequestComponent } from './request/request.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {RouterModule, Routes} from '@angular/router';
import { LoadContractComponent } from './contracts/load-contract/load-contract.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ContractFilterPipe } from './shared/contract-filter.pipe';
import { NgbdModalContentComponent } from './shared/ngbd-modal-content/ngbd-modal-content.component';
import { HotelComponent } from './hotel/hotel.component';


const appRoutes: Routes = [
  {
    path: 'request',
    component: RequestComponent
  },
  {
    path: 'contracts',
    component: ContractsComponent,
    children: [
      {
        path: 'add-contract',
        component: LoadContractComponent
      },
    ],
  },
  {
    path: '',
    component: RequestComponent,
    pathMatch: 'full'
  },
  {
    path: 'hotels',
    component: HotelComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    ContractsComponent,
    NavigationComponent,
    RequestComponent,
    NotFoundComponent,
    LoadContractComponent,
    ContractFilterPipe,
    NgbdModalContentComponent,
    HotelComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    NgbdModalContentComponent
  ]
})

export class AppModule { }
