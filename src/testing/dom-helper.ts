import {ComponentFixture} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

export class DOMHelper<T> {
  private fixture: ComponentFixture<T>;
  constructor(fixture: ComponentFixture<T>) {
    this.fixture = fixture;
  }

  singleText(tagName: string): string {
    const element = this.fixture.debugElement.query(By.css(tagName));
    if (element) {
      return element.nativeElement.textContent;
    }
  }

  count(tagName: string): number {
    const elements = this.fixture.debugElement.queryAll(By.css(tagName));
    return elements.length;
  }

  countText(tagName: string, text: string): number {
    const elements = this.fixture.debugElement.queryAll(By.css(tagName));
    return elements.filter(element => element.nativeElement.textContent === text).length;
  }

  findButton(buttonText: string): string  {
    let buttonToFind;
    this.findAll('button').forEach(button => {
      const buttonElement: HTMLButtonElement = button.nativeElement;
      if (buttonElement.textContent === buttonText) {
        buttonToFind = buttonElement.textContent;
      }
    });
    return buttonToFind;
  }

  clickButton(buttonText: string) {
    this.findAll('button').forEach(button => {
      const buttonElement: HTMLButtonElement = button.nativeElement;
      if (buttonElement.textContent === buttonText) {
        buttonElement.click();
      }
    });
  }

  clickButtonAtIndex(buttonText: string, index: number) {
    const buttonList = this.findAll('button');
    return buttonList[index].nativeElement.click();
  }

  clickButtonLink(buttonText: string) {
    this.findAll('a').forEach(button => {
      const buttonElement = button.nativeElement;
      if (buttonElement.textContent === buttonText) {
        buttonElement.click();
      }
    });
  }


  findAll(tagName: string) {
    return this.fixture.debugElement.queryAll(By.css(tagName));
  }

  findTextByIndex(tagName: string, index: number) {
    const elementList = this.findAll(tagName);
    return elementList[index].nativeElement.textContent;
  }


}
